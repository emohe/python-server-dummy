## **Python server dummy**

This repository has been built just as an example to make it easy to plug in a new CBIR system.
All the functions are free to change and all the code can be adapted and reused in order to
plug in a new system.

<!-- > The full code of the CBIR used is available in https://github.com/imatge-upc/salbow. -->

### How needs the project to be structured?

The project provides a python server that allows to merge your code with the whole project.
To do that, a `main server` file is created and it will be the one which connects your code with
the `nodejs` server.

  * This repository uses a random (dummy) search engine. The engine is implemented in a python class (dummy_retrieval/dummy.py) with four main functionalities:

      - `get_rank_for_dataset_image`:
          Given an image_id from the dataset, it returns a list of random images to display.
      - `get_rank_for_image_array`:
          Given numpy array containing an image (python-opencv format), it returns a list of random images to display.
      - `query_expansion`:
          Given a list of relevant image_ids, it performs average query expansion and returns a list of random images to display.
      - `annotations`:
        Given a list of relevant image_ids and unrelevant image_ids, it trains a linear SVM and returns a list of relevant images to display.


      > All this functionalities can be modified. They are just given as an examples in order to ease the process of plugging in a new CBIR system.

  * There is also 2 already existing methods that you can modify in order to establish
  the connection with the UI and thus, the node-server. You can also add other methods
  if required by following the specific instructions given in the self-generated documentation.

  > See the last section of README to automatically generate the documentation.

      - `postServer` method

          This method receives all the parameters needed to compute the ranking
          of a query given.

          First of all, the method is defined like:

          ```py
          def postServer(self, id_img, url, encoded_image, dataset, path):
          ```

          Then, a code is done in order to compute the ranking. You can put here your
          own code but it should return a `json structure` with the keys `IdSequence` and `Image` as
          it can be seen in the following example.

          ```json
          [
            {
                "IdSequence": "0",
                "Image": "paris_general_002391"
            },
            {
                "IdSequence": "1",
                "Image": "paris_eiffel_000128"
            },
                 ...

            {
                "IdSequence": "5011",
                "Image": "paris_invalides_000541"
            }
          ]
          ```
          > Upper and lower case matters in this structures.

      - `postFeedback_And_Update` method

          This method receives all the parameters needed to get the feedback of the user.
          You should return the updated ranking after computing the desired experiments.

          First of all, a method is defined like:

          ```py
          def postFeedback_And_Update(self, id_img, url, encoded_image, dataset, path, similar_list, mode):

          ```

          Then, you can code whatever you need in order to return a json object with
          the following shape (it changes depending on the mode):

          * QE MODE:

          ```json
          {
              "json": [{},...,{}],
              "initial": 0.700725,
              "final": 0.639502
           }
          ```

           * Annotation mode
           ```json
           {
              "json": [{},...,{}],
              "success": true
           }
          ```

          > The `json` field will contain the ranking updated and it should have the same
          structure as in the method above.
