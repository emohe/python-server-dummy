import base64
import cv2
import urllib
import os
import json
import numpy as np
from IPython import embed


def fix_image_list_format(similar_list, dataset):
    # ================================ FIX THE FORMAT ==================
    # similarity list - remove extension...
    similar_list = [ str(n).split('.')[0] for n in similar_list]

    # remove black spaces... [not sure why they have blank spaces at the beginning]
    similar_list = [ n.replace(" ", "") for n in similar_list]

    # change __ from instre....
    if dataset == "instre":
        similar_list = [s.replace("__", "/") for s in similar_list]
    return similar_list


def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += b'='* (4 - missing_padding)
    return base64.decodestring(data)

def process_url(url):
    #decode url
    resp = urllib.urlopen(url)
    ima_path = np.asarray(bytearray(resp.read()), dtype="uint8")
    ima = cv2.imdecode(ima_path, cv2.IMREAD_COLOR)
    print("Shape image decoded from url {}".format( ima_path.shape ))
    return ima

def read_image(url):
    return cv2.imread(url)


def create_json_list( path_ranks, id_ima, list_names, dataset, top_n=5000):
    """
    create json rank for image id and list of image from a particular dataset
    """

    path_ranks = os.path.join(path_ranks, id_ima)
    sub = os.path.dirname(path_ranks)
    name = os.path.basename(path_ranks).split(".")[0]
    if not os.path.exists(sub):
        os.makedirs(sub)
    json_file =  os.path.join(  sub,"{}.json".format(name) )
    #check if subfolder exists

    if top_n is None:
        top_n = list_names.shape[0]

    with open( json_file, 'wb' ) as f:
        list_names = list_names[:top_n]
        N = list_names.shape[0]
        for i, name in enumerate(list_names):
            line=''
            name = name.split('.')[0]

            # IDEA: modified by paula
            if dataset == "instre":
                name = name.replace("/", "__")

            if i == 0:
                line+="[{\n\"IdSequence\" :\"%i\",\n" % ( i)
            else:
                line+="{\n\"IdSequence\" :\"%i\",\n" % ( i)
            line+="\"Image\":\"%s\"\n" % name
            if i == N-1:
                line+="}]"
            else:
                line+="},\n"
            # write
            f.write(line)
    return json_file
