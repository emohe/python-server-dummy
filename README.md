# Example of a Dummy Retrieval Search engine
This repository shows the main functionalities of the python serves for the UI
developed in [node-server](https://github.com/paulagd/node-server)

Each image is represented by a random descriptor od 100 dimensions.
Cosine similarity on L2-norm descriptors is used to generate the ranks.
A Linear SVM is trained on possitive and negative annotations to refine the optained
results when used the Annotation mode described in [react-visualization-tool-CBIR](https://github.com/paulagd/react-visualization-tool-CBIR).
An example of an implementation for the average QE is provided for the QE mode of the UI.


### Prerequisits

  - Python 2.7
  - Nodejs v6.11.3
  - npm v5.4.2

> In the [reactjs repository](https://github.com/paulagd/react-visualization-tool-CBIR) you can find the steps to install them.


## Requirements

(Tested on Ubuntu 16.04 system and MacOS Sierra)

* Create a virtualenv using python2.7 as explained in the [reactjs repository](https://github.com/paulagd/react-visualization-tool-CBIR).

```
 pip install -r requirements.txt
```

* Make sure you are using tensorflow order for the images. Edit '~/.keras/keras.json'
```
{    "image_dim_ordering": "tf", "epsilon": 1e-07, "floatx": "float32", "backend": "tensorflow"}
```

### Repositories related:
>
> * [React visualization tool for CBIR](https://github.com/paulagd/react-visualization-tool-CBIR)
> * [Nodejs server](https://github.com/paulagd/node-server)
> * [python-server-salbow](https://bitbucket.org/emohe/python-server-salbow/src/master/)


### Execution

* To run the code run:
```
python main.py    
```

### Documentation

Install apidoc
```
npm install -g apidoc

```

Run documentation generation script:
```
apidoc -o documentation -e node_modules

```

> Open the file 'index.html' stored in the folder 'documentation' to see how to customize the system.
