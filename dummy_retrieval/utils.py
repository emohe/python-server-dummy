import numpy as np
from IPython import embed

def compute_similarity(targets, queries, l2norm=True):
    """
    This function computes the distance between targets and queries.
    we use the cosine similarity between l2-norm vectors

    returns (N_target,N_queries) similarity ranks
    """
    if len(queries.shape)<2:
        queries = np.expand_dims(queries, axis=0)
    similarities = targets.dot(queries.T)

    return similarities


def compute_ranks(similarity_matrix):
    """
    Compute ranks based on a similarity matrix
    """
    # use cosine similarity

    # check if using sparse vecr
    ranks = []
    N_queries = similarity_matrix.shape[1]
    for i in range(N_queries):
        rank = np.argsort( similarity_matrix[:,i] )[::-1]
        ranks.append(rank)
    return np.array(ranks).T


def compute_random_descriptors(list_images, n_dimension=100):
    """
    Generate a random n_dimensional descriptors for a given list of images.

    """
    N = list_images.shape[0]
    return np.random.random((N,n_dimension))


def process_image(ima, n_dimension=100):
    """
    This function should process a new image. It generates random descriptors
    for it instead...

    """
    return np.random.random(n_dimension)
