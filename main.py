import zerorpc
import requests
import logging
import sys
import json
import os

from dummy_retrieval import Dummy_engine
from evaluate import Instre_evaluator, OXPAR_evaluator
from python_utilities.utilities import (process_url,
                                        decode_base64,
                                        create_json_list,
                                        fix_image_list_format)

from IPython import embed


class MainServer(object):

    """
    @api {post} /sendFeedback_receiveRanking/:id  postFeedback_And_Update()
    @apiName postFeedback_And_Update
    @apiGroup Main Functions

    @apiParam {String} id_img Id of the query
    @apiParam {String} [url] Url of the image introduced to the system.
    @apiParam {String} [encoded_image] Encoded image uploaded to the system.
    @apiParam {String} dataset Dataset given
    @apiParam {String} [path] Path given in the case of 'complicated' datasets as 'instre'.
    @apiParam {String} similar_list List of the queries selected for the query expansion.
    @apiParam {String} mode Mode selected to be in use.

    @apiDescription This function collects the annotations of the users. Depending on the
    mode selected by the user, it computes multiquery in order to improve the accuracy of
    the system (if in QE mode by computing the average of the images) or it trains an SVM
    (if its in Annotation mode). However, this is just our example and they can be modified.

    @apiExample {json} Request (QE mode)
          {
              "similar_list": [" paris_general_001620.jpg", " paris_general_002391.jpg", " paris_eiffel_000128.jpg"],
              "dataset":"paris",
              "mode": "q",
              "url": null,
              "encoded_image": null,
              "path": null
          }

     @apiExample {json} Request (Annotation mode)
          {
             "similar_list": {
                               "positive": ["paris_general_001620.jpg", "paris_eiffel_000128.jpg"],
                               "negative": ["paris_general_000144.jpg", "paris_general_002444.jpg"]
                },
              "dataset": "paris",
              "mode": "a",
              "url": null,
              "encoded_image": null,
              "path": null
          }

    """

    def __init__(self, eval_flag=False):
        # init search engine dictionary...
        self.search_engine = {}
        self.path_out = {}
        # max images to display
        self.top_n = 5000
        self.evaluator = {}
        self.eval_flag = eval_flag

    def _init_dataset(self, dataset):
        if dataset not in self.search_engine.keys():
            # INIT SEARCH ENGINE
            self.search_engine[dataset] = Dummy_engine(dataset)
            # creat output path for ranks
            path_out_ranks = os.path.join('ranks_dummy',dataset)
            if not os.path.exists(path_out_ranks):
                os.makedirs(path_out_ranks)

            # INIT PATH OUT for visual.
            self.path_out[dataset] = path_out_ranks

            # INIT EVAL
            if dataset == 'instre':
                self.evaluator[dataset] = Instre_evaluator()
            else:
                self.evaluator[dataset] = OXPAR_evaluator(dataset)

    def _evaluate_result(self, dataset, id_img, list_names):
        # [NEW!] example of how to evaluate ===============================
        # Maybe this whould be done when we explicitly ask for that?

        # ranks = self.search_engine[dataset].ranks
        mAP = 0
        ap = 0

        new_ranks = self.search_engine[dataset].new_ranks

        # Save the old accuracy
        if os.path.isfile('temp_ap.txt'):
            accuracies = []
            f = open('temp_ap.txt','rw+')
            mAP = f.readline()
            ap = f.readline()

        new_mAP = self.evaluator[dataset].compute_map(new_ranks)
        new_ap = self.evaluator[dataset].compute_ap_for_image(id_img, list_names)

        if new_ap is None:
            new_ap = -1

        with open('temp_ap.txt', 'wb') as f:
            f.write(str(new_mAP))
            f.write('\n')
            f.write(str(new_ap))

        return ap, mAP, new_ap, new_mAP


    def postFeedback_And_Update(self, id_img, url, encoded_image, dataset, path, similar_list, mode):
        # default action
        if dataset.lower() == 'instre' and (url is None):
            # id_img = path.split(".")[0]
            id_img = id_img.replace("__", "/").split('.')[0]

        # remove extension and convert to str
        id_img = str(id_img.split('.')[0])

        if 'unicode' in str(type(encoded_image)):
            url = None

        # init search engine
        self._init_dataset(dataset)

        result = {}
        ima = None

        # [NEW] - Allow images from outside dataset=============================
        if encoded_image is not None:
            print("Uploading image...")
            # CHECK THAT THIS IS ACTUALLY WORKING
            ima = decode_base64(encoded_image)

        elif url is not None:
            print("processing url...")
            ima = process_url(url)
        # ======================================================================

        if mode=='q':
            similar_list = fix_image_list_format(similar_list, dataset)
            list_names = self.search_engine[dataset].query_expansion( id_img, similar_list, ima=ima )
        else:
            for opt in ['positive', 'negative']:
                similar_list[opt] = fix_image_list_format(similar_list[opt], dataset)

            list_names = self.search_engine[dataset].annotations( id_img, similar_list, ima=ima)

        # addapt list for the server format to display
        json_file = create_json_list(self.path_out[dataset],
                                     "feedback", list_names,
                                     dataset, top_n=self.top_n)

        with open(json_file) as data_file:
           data = json.load(data_file)
        # evaluate
        if self.eval_flag:
            ap, mAP, new_ap, new_mAP = self._evaluate_result(dataset, id_img, list_names)

            print '.......................'
            print '.......................'
            print "Query-{} AP from AP={} to AP={}".format( id_img, ap, new_ap )
            print "Global mAP from {} to {}".format(mAP,new_mAP)

        result = {'json': {}, 'initial':0 , 'final': 0, 'previous_mAP': 0, "mAP": 0 , "success":False}

        if self.eval_flag:
            result['initial'] =  round(float(ap),4)
            result['final'] = round(float(new_ap),4)
            result['previous_mAP'] = round(float(mAP),4)
            result['mAP'] = round(float(new_mAP),4)
            result['success'] = True

        result['json'] = data
        return result

    """
    @api {post} /getRankinById/:id  postServer()
    @apiName postServer
    @apiGroup Main Functions

    @apiParam {String} id_img Id of the query
    @apiParam {String} [url] Url of the image introduced to the system.
    @apiParam {String} [encoded_image] Encoded image uploaded to the system.
    @apiParam {String} dataset Dataset given
    @apiParam {String} [path] Path given in the case of 'complicated' datasets as 'instre'.

    @apiDescription This function generates the ranking of similarity given a query image.
    Depending on the dataset that is selected by the user, we compute one specific funtion.
    However, it is again just an example that can be modified. In the example, the datasets
    oxford, paris and instre are using the `dummy retrieval` as an example of a search engine
    in order to compute the rankings. Feel free to use whichever suits you or either
    introduce your own system.


    @apiExample {json} Request
          {
              "dataset":"paris",
              "url": null,
              "encoded_image":null,
              "path": null
          }
    """
    def postServer(self, id_img, url, encoded_image, dataset, path):

        # remove extension and convert to str
        id_img = str(id_img.split('.')[0])

        if (dataset.lower() == 'oxford' or dataset.lower() == 'paris' or dataset.lower() == 'instre') :

            if dataset.lower() == 'instre' and (url is None):
                id_img = id_img.replace("__", "/").split('.')[0]

            if 'unicode' in str(type(encoded_image)):
                url = None

            json_file = None

            # init search engine
            self._init_dataset(dataset)

            # Not sure the upload image mode is working
            if encoded_image is not None:
                print("Uploading image...")
                # CHECK THAT THIS IS ACTUALLY WORKING
                ima = decode_base64(encoded_image)
                list_names = self.search_engine[dataset].get_rank_for_image_array(ima)

            elif url is not None:
                print("processing url...")
                ima = process_url(url)
                list_names = self.search_engine[dataset].get_rank_for_image_array(ima)

            # processing image from inside the dataset...
            elif id_img is not "unknown_id":
                list_names = self.search_engine[dataset].get_rank_for_dataset_image(id_img)
            else:
                list_names = None
                print("New case....")


            # evaluate
            if self.eval_flag:
                ap, mAP, new_ap, new_mAP  = self._evaluate_result(dataset, id_img, list_names)

                print '.......................'
                print "Query-{} AP from AP={} to AP={}".format( id_img, ap, new_ap )
                print "Global mAP from {} to {}".format(mAP,new_mAP)


            # addapt list for the server format to display
            json_file = create_json_list(self.path_out[dataset],
                                         id_img, list_names,
                                         dataset, top_n=self.top_n)

            with open(json_file) as data_file:
                data = json.load(data_file)

            result = {'json': data, 'mAP': round(float(new_mAP),3)}
            print('---------- RANKIN SENT ---------')
            return result



        else:
            raise ValueError('There is no qimList generated for this dataset.')
            return ValueError('There is no qimList generated for this dataset.')


import argparse

parser = argparse.ArgumentParser(description='Python-server')
parser.add_argument('--evaluate', type=bool, default=True,
                    help='flag to evaluate rankings')



if __name__ == '__main__':
    args = parser.parse_args()
    logging.basicConfig()
    s = zerorpc.Server(MainServer(eval_flag=args.evaluate))
    s.bind("tcp://0.0.0.0:4243")
    print("Python server listening on: 4243")
    s.run()
    sys.stdout.flush()
