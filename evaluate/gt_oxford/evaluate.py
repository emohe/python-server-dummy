#!/usr/bin/python
import os, glob
import numpy as np

# queries
query_names = ["all_souls", "ashmolean", "balliol","bodleian", "christ_church", "cornmarket","hertford","keble","magdalen","pitt_rivers","radcliffe_camera"]

aps = []
for q_name in query_names:
    cmd = "./compute_ap {}_1 /home/eva/workspace/cvpr16/src/lists/{}.txt > tmp.txt".format(q_name, q_name)
    os.system( cmd )
    ap = np.loadtxt("tmp.txt")
    print "{}".format(ap)
    aps.append(ap)

print 
print np.average(aps)
