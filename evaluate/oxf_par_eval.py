import os
import json
import numpy as np
import shutil
from IPython import embed


class Evaluator():
    def __init__(self, dataset):
        self.q_keyframes = np.loadtxt( "indices/{}/qimlist.txt".format(dataset),\
                                      dtype='str', delimiter='\n' )
        self.q_topics = np.loadtxt( "indices/{}/topics.txt".format(dataset),\
                                      dtype='str', delimiter='\n' )
        self.keyframes = np.loadtxt( "indices/{}/imlist.txt".format(dataset),\
                                      dtype='str', delimiter='\n' )

        self.keyframes = np.array([n.split('.')[0] for n in self.keyframes])
        self.q_keyframes = np.array([n.split('.')[0] for n in self.q_keyframes])

        self.dataset = dataset

        # create path to store txt with ranks
        self.path_out = os.path.join( 'evaluate/ranks_{}'.format(dataset))
        self._clear_folder()

        # check that compute_ap exists
        if not os.path.exists("/evaluate/compute_ap"):
            # download compute_ap
            #cmd = "curl www.robots.ox.ac.uk/~vgg/data/oxbuildings/compute_ap.cpp -o 'evaluate/compute_ap.cpp'"
            #os.system(cmd)
            # compile
            cmd = "g++ 'evaluate/compute_ap.cpp' -o 'evaluate/compute_ap'"
            os.system(cmd)


    def _clear_folder(self):
        if os.path.exists(self.path_out):
            shutil.rmtree(self.path_out)
            os.makedirs(self.path_out)
        else:
            os.makedirs(self.path_out)

    def compute_map(self, ranks):
        self.generate_txt(ranks)
        mAP = self.compute_ap_oxf()
        return mAP

    def compute_ap_for_image(self, id_img, list_names):
        if id_img not in self.q_keyframes:
            return None
        else:
            # get id for query
            idx = np.where( id_img == self.q_keyframes )[0][0]
            # get topic name
            topic_name = self.q_topics[idx]

            # create list txt
            path_file =  os.path.join(self.path_out, topic_name+'.txt')
            fid = open( path_file, 'w' )
            for name in list_names:
                fid.write("{}\n".format(name))

            # evaluate list
            cmd = "./evaluate/compute_ap '{}/{}' '{}'".format( "evaluate/gt_{}".format(self.dataset), topic_name, path_file)
            ap = float(os.popen(cmd).read())
            return ap

    def generate_txt(self, ranks):
        self._clear_folder()
        for i, name in enumerate(self.q_topics):
            fid = open(  os.path.join(self.path_out, name+'.txt'), 'w' )
            for k in range(ranks.shape[0]):
                name = self.keyframes[ranks[k,i]]
                fid.write("{}\n".format(name))
            fid.close()

    def compute_ap_oxf(self):
        """
        Generate AP per rankfile
        params:
            path_ranks: where .txt files are allocated
            gt_path: where OX/PAR queries are allocated
        """
        results = {}
        for rank in os.listdir(self.path_out):
            q_id = os.path.basename( rank ).split('.')[0]
            cmd = "./evaluate/compute_ap '{}/{}' '{}/{}'".format( "evaluate/gt_{}".format(self.dataset), q_id, self.path_out, rank )
            ap = float(os.popen(cmd).read())
            results[ os.path.basename( rank ).split('.')[0] ] = ap
        print("mAP = {}".format(np.mean(results.values())))
        return np.mean(results.values())
